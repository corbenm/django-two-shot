from django.urls import path
from accounts.views import loginUser, logoutUser, signUp

urlpatterns = [
    path("login/", loginUser, name="login"),
    path("logout/", logoutUser, name="logout"),
    path("signup/", signUp, name ="signup"),
]
